package comptebanque;

public class Compte {
	int solde;
	
	public Compte()
	{
		this.solde = 0;
	}
	
	public Compte(int solde)
	{
		if(solde >= 0)
		{
			this.solde = solde;
		}
	}
	
	public int getSolde()
	{
		return this.solde;
	}
	
	public boolean removeMoney(int n)
	{
		if(n <= 0)
		{
			return false;
		}
		if(this.getSolde() > n)
		{
			this.solde -= n;
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void addMoney(int n)
	{
		if(n > 0)
		{
			this.solde += n;
		}	
	}
	
	public boolean transferMoneyTo(Compte c, int n)
	{ 
		if(n <= 0)
		{
			return false;
		}
		if(this.removeMoney(n))
		{
			c.addMoney(n);
			return true;
		}
		else
		{
			return false;
		}
	}
}
	










